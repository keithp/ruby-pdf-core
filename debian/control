Source: ruby-pdf-core
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby-pdf-inspector,
               ruby-pdf-reader,
               ruby-rspec
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-pdf-core.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-pdf-core
Homepage: https://github.com/prawnpdf/pdf-core
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-pdf-core
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends},
Breaks: ruby-prawn (<< 2.4~)
Description: Ruby library to render PDF documents
 PDF::Core is a pure Ruby library to render PDF documents. It supports
 several PDF features, such as among others:
  * low-level annotation
  * istream objects and stream filters
  * NameTree
  * object repository
  * object serialization
  * indirect objects
  * page geometries
 .
 It is used internally by Prawn (provided in the ruby-prawn package),
 a Ruby PDF generation library.
